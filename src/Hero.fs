namespace Kandddinsky


open Fable.React
open Fable.React.Props


module Hero =

  let view dispatch model =
    div [ Class "py-24 px-2 sm:px-6" ]
      [ 
        div [ Class "relative rotate-3" ]
          [
            img [ Class "mx-auto max-w-5xl w-full"
                  Alt "KanDDDinsky in Berlin from October 31st until November 2nd"
                  Src "./img/design/hero.svg" ]
            div [ Class "absolute inset-0 flex flex-col items-center justify-center font-bold text-9xl" ]
              [
                ["28—30" |> ofString] |> span [ Class "text-cyan-600" ] 
                span [] 
                  [
                    [ "OCT" |> ofString ] |> span [ Class "text-zinc-800" ] 
                    [ "24" |> ofString ] |> span [ Class "text-amber-500" ] 
                  ]

              ]
          ]
      ]