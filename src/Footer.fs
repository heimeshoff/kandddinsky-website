namespace Kandddinsky


open Fable.React
open Fable.React.Props
open SVGs



module Footer =


  let legal_link label target dispatch = 
    span [ Class "cursor-pointer hover:text-gray-300"
           OnClick (fun _ -> Navigate_to target |> dispatch) 
         ]
      [ str label ]  


  let social_link title url svg=
    a [ Href url
        Target "_blank"
        Class "text-gray-100 hover:text-gray-300" ]
      [ span [ Class "sr-only" ]
          [ str title ]
        svg
      ] 


  let legal_links dispatch = 
    div [ Class "w-full flex flex-col sm:flex-row items-center justify-center sm:space-x-6 space-y-2 sm:space-y-0  font-semibold text-gray-200" ]
      [
        legal_link "Imprint" Imprint dispatch
        legal_link "Photography" Photography dispatch
        legal_link "GDPR" GDPR dispatch
        legal_link "Code of conduct" Code_of_conduct dispatch
        a [ Class "font-semibold text-gray-200 hover:text-gray-300 cursor-pointer"
            Href "https://kandddinsky.us4.list-manage.com/subscribe/post?u=c7494cdfb662e6fff2ca03757&id=920a7a2b3f"
            Target "_blank" ]
          [ "Subscribe!" |> ofString ]
      ]


  let social_links = 
    div [ Class "flex items-center space-x-6" ]
      [
        social_link "Twitter" "https://twitter.com/kandddinsky" svg_twitter
        social_link "Facebook" "https://www.facebook.com/KanDDDinsky/" svg_facebook
        social_link "Youtube" "https://www.youtube.com/channel/UCJCpnslPdb_Dl8DKokXC3HA" svg_youtube
        social_link "Instagram" "https://www.instagram.com/kandddinsky" svg_instagram
      ]


  let copyright = 
    div [ Class "font-semibold text-base text-gray-400 flex flex-wrap" ]
      [ 
        [ "© KanDDDinsky UG haftungsbeschränkt." |> ofString ] |> span [ Class "mr-1" ]
        [ "All Rights Reserved." |> ofString ] |> span []
      ]


  let view dispatch =
    div [ Class "bg-gray-500 py-12 px-6 flex flex-col justify-between items-center space-y-6" ]
      [
        legal_links dispatch
        social_links
        copyright
      ]