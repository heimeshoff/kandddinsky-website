namespace Kandddinsky


open Fable.React
open Fable.React.Props



module Venue =


  let map = 
    div [ Class "mapouter sm:w-1/2 flex justify-center items-center" ]
      [ div [ Class "gmap_canvas" ]
          [ iframe [ Id "gmap_canvas"
                     Src "https://maps.google.com/maps?q=2880%20Broadwahttps://www.google.com/maps/place/Berlin+Marriott+Hotel/@52.5109109,13.3761401,15z/data=!4m2!3m1!1s0x0:0xdb5b67550de11f2d?sa=X&ved=2ahUKEwjP9vnfktD5AhWRX8AKHc70B9QQ_BJ6BAh6EAUy,%20New%20York&t=&z=15&ie=UTF8&iwloc=&output=embed"
                     FrameBorder 0
                     Scrolling "no"
                     MarginHeight 0.
                     MarginWidth 0. ]
              [ ]
          ] 
      ]


  let description =
    div [ Class "shadow-lg py-8 px-12 bg-gradient-to-br from-slate-50 to-blue-50 z-50 flex flex-col sm:flex-row items-start" ]
      [
        div [ Class "text-gray-700 font-medium text-justify py-2 z-auto sm:w-1/2"]
          [
            p [  Class "" ] [ "KanDDDinsky 2023 will take place at the Berlin Marriot Hotel. We highly recommend to get a room in the venue hotel for the most indulging KanDDDinsky-experience." |> ofString ]
            a [ Href "https://www.marriott.com/en-us/hotels/bermc-berlin-marriott-hotel/overview/"
                Target "_blank"
                Class "text-blue-500 hover:text-blue-700 text-lg" ]
              [ "Berlin Marriot Hotel" |> ofString ]             

            p [ Class "mt-4" ] [ "For accomodation on a budget we recommend the Motel One Potsdamer Platz. Everything a modern business traveler could want and just down the street from the venue." |> ofString ]
            a [ Href "https://www.motel-one.com/en/hotels/berlin/hotel-berlin-potsdamer-platz/"
                Target "_blank"
                Class "text-blue-500 hover:text-blue-700 text-lg" ]
              [ "Motel One Potsdamer Platz" |> ofString ]             
          ]
        map

      ]        

  let view dispatch model =
    div [ Id "venue" 
          Class "mx-auto max-w-5xl w-full py-24" ]
      [ 
        [ "Venue" |> ofString ] |> div [ Class "text-center text-6xl font-bold text-gray-700 mb-8" ]

        description
      ]
