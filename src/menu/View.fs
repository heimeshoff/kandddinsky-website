namespace Kandddinsky.Menu



module View =

  open Fable.React
  open Fable.React.Props

  let youtube =
    a [ Class "my-auto p-2 text-2xl text-gray-700 border-b-2 border-transparent hover:border-orange-400 transition-colors"
        Href "https://www.youtube.com/channel/UCJCpnslPdb_Dl8DKokXC3HA" 
        Target "_blank" ]
      [
        "Videos" |> ofString
      ]


  let hamburger_menu_button dispatch onClickMsg menu_open =
    button [  Type "button"
              Class "bg-white inline-flex items-center justify-center py-2 px-4 text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none"
              OnClick (fun _ -> dispatch onClickMsg )
              HTMLAttr.Custom ("aria-controls", "mobile-menu")
              AriaExpanded false ]
      [ span [ Class "sr-only" ]
          [ str "Open main menu" ]
        svg [ Class (sprintf "%s h-6 w-6" (if menu_open then "hidden" else "block") )
              HTMLAttr.Custom ("xmlns", "http://www.w3.org/2000/svg")
              HTMLAttr.Custom ("fill", "none")
              HTMLAttr.Custom ("viewBox", "0 0 24 24")
              HTMLAttr.Custom ("stroke-width", "2")
              HTMLAttr.Custom ("stroke", "currentColor")
              HTMLAttr.Custom ("aria-hidden", "true") ]
          [ path [  HTMLAttr.Custom ("stroke-linecap", "round")
                    HTMLAttr.Custom ("stroke-linejoin", "round")
                    HTMLAttr.Custom ("d", "M4 6h16M4 12h16M4 18h16") ]
              [ ] ]
        svg [ Class (sprintf "%s h-6 w-6" (if menu_open then "block" else "hidden") )
              HTMLAttr.Custom ("xmlns", "http://www.w3.org/2000/svg")
              HTMLAttr.Custom ("fill", "none")
              HTMLAttr.Custom ("viewBox", "0 0 24 24")
              HTMLAttr.Custom ("stroke-width", "2")
              HTMLAttr.Custom ("stroke", "currentColor")
              HTMLAttr.Custom ("aria-hidden", "true") ]
          [ path [  HTMLAttr.Custom ("stroke-linecap", "round")
                    HTMLAttr.Custom ("stroke-linejoin", "round")
                    HTMLAttr.Custom ("d", "M6 18L18 6M6 6l12 12") ]
              [ ] ]                    
      ]


  let menu_points tickets cfp agenda workshops venue sponsoring = 
    [ 
      tickets
      // workshops
      cfp
      // agenda
      youtube
      // venue
      // sponsoring
    ]


  let popout_menu tickets cfp agenda workshops venue sponsoring menu_open = 
    div [ Class (sprintf "%s absolute -ml-4 mt-16 p-4 w-full bg-white shadow-md flex-col space-y-2" (if menu_open then "flex" else "hidden" ))]
      (menu_points tickets cfp agenda workshops venue sponsoring)


  let mobile_menu dispatch home tickets cfp agenda workshops venue sponsoring menu_open =
    div [ Class "pl-4 mx-auto max-w-6xl flex lg:hidden flex-row justify-between relative" ]
      [
        home

        hamburger_menu_button
          dispatch
          (if menu_open then Close_Menu else Open_Menu)
          menu_open

        popout_menu tickets cfp agenda workshops venue sponsoring menu_open
      ]


  let desktop_menu home tickets cfp agenda workshops venue sponsoring =
    div [ Class "hidden px-6 mx-auto max-w-6xl lg:flex flex-row justify-between" ]
      [
        home
        div [ Class "flex flex-row space-x-4"]
          (menu_points tickets cfp agenda workshops venue sponsoring)
      ]


  let render dispatch transparent_background home tickets cfp agenda workshops venue sponsoring model =
    let background = 
      if transparent_background then "bg-white/0" else "bg-white/75 backdrop-blur-md shadow-md"

    div [ Class (sprintf "z-30 w-full h-16 fixed transition duration-300 %s" background) ]
      [
        mobile_menu dispatch home tickets cfp agenda workshops venue sponsoring model.menu_open
        desktop_menu home tickets cfp agenda workshops venue sponsoring
      ]