namespace Kandddinsky.Menu


type Model = {
  menu_open: bool
}


type Msg = 
  | Open_Menu
  | Close_Menu
