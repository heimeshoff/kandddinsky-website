namespace Kandddinsky.Menu


module State = 

  open Elmish


  let init = 
    {
      menu_open = false
    } 


  let update msg model = 
    match msg with
    | Open_Menu -> { model with menu_open = true }, Cmd.none
    | Close_Menu -> { model with menu_open = false }, Cmd.none