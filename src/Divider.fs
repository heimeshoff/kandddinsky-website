namespace Kandddinsky



module Divider =

  open Fable.React
  open Fable.React.Props


  let first = 
    div [ Class "flex flex-row overflow-hidden"]
      [
        img [ Class "flex-1" ; Src "./img/2022/divider-4.png" ]
        img [ Class "flex-1" ; Src "./img/2022/divider-6.png" ]
        img [ Class "flex-1" ; Src "./img/2022/divider-5.png" ]
      ]

  let second = 
    div [ Class "flex flex-row overflow-hidden"]
      [
        img [ Class "flex-1" ; Src "./img/2022/divider-2.png" ]
        img [ Class "flex-1" ; Src "./img/2022/divider-1.png" ]
        img [ Class "flex-1" ; Src "./img/2022/divider-3.png" ]
      ]