namespace Kandddinsky


open Fable.React
open Fable.React.Props



module Code_of_conduct =


  let view dispatch model =
    div [ Class "py-12 px-6 sm:px-8" ]
      [ 
        div [ Class "mx-auto max-w-2xl text-gray-700 mt-16" ]
          [
            [ "Code of conduct" |> ofString ] |> h2 [ Class "font-bold text-2xl  my-2" ] 
            p [ Class "text-justify" ]
              [ "All attendees, speakers, sponsors and volunteers at our conference are required to agree with the following code of conduct. Organisers will enforce this code throughout the event. We expect cooperation from all participants to help ensure a safe environment for everybody." |> ofString ]

            [ "Need Help?" |> ofString ] |> p [ Class "font-semibold text-xl mt-8" ]
            [ "You have our contact details in the emails we've sent." |> ofString ] |> p [ ]

            [ "The Quick Version" |> ofString ] |> p [ Class "font-semibold text-xl mt-8" ]
            p [ Class "text-justify" ]
              [ "Our conference is dedicated to providing a harassment-free conference experience for everyone, regardless of gender, gender identity and expression, age, sexual orientation, disability, physical appearance, body size, race, ethnicity, religion (or lack thereof), or technology choices. We do not tolerate harassment of conference participants in any form. Sexual language and imagery is not appropriate for any conference venue, including talks, workshops, parties, Twitter and other online media. Conference participants violating these rules may be sanctioned or expelled from the conference without a refund at the discretion of the conference organisers." |> ofString ]

            [ "The Less Quick Version" |> ofString ] |> p [ Class "font-semibold text-xl mt-8" ]
            p [ Class "text-justify" ]
              [ "Harassment includes offensive verbal comments related to gender, gender identity and expression, age, sexual orientation, disability, physical appearance, body size, race, ethnicity, religion, technology choices, sexual images in public spaces, deliberate intimidation, stalking, following, harassing photography or recording, sustained disruption of talks or other events, inappropriate physical contact, and unwelcome sexual attention." |> ofString ]
            p [ Class "text-justify mt-2" ]
              [ "Participants asked to stop any harassing behavior are expected to comply immediately." |> ofString ]
            p [ Class "text-justify mt-2" ]
              [ "Sponsors are also subject to the anti-harassment policy. In particular, sponsors should not use sexualised images, activities, or other material. Booth staff (including volunteers) should not use sexualised clothing/uniforms/costumes, or otherwise create a sexualised environment." |> ofString ]
            p [ Class "text-justify mt-2" ]
              [ "If a participant engages in harassing behavior, the conference organisers may take any action they deem appropriate, including warning the offender or expulsion from the conference with no refund." |> ofString ]
            p [ Class "text-justify mt-2" ]
              [ "If you are being harassed, notice that someone else is being harassed, or have any other concerns, please contact a member of conference staff immediately. Conference staff can be identified as they'll be wearing branded clothing and/or badges." |> ofString ]
            p [ Class "text-justify mt-2" ]
              [ "Conference staff will be happy to help participants contact hotel/venue security or local law enforcement, provide escorts, or otherwise assist those experiencing harassment to feel safe for the duration of the conference. We value your attendance." |> ofString ]
            p [ Class "text-justify mt-2" ]
              [ "We expect participants to follow these rules at conference and workshop venues and conference-related social events." |> ofString ]
          ]
      ]