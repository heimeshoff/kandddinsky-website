namespace Kandddinsky


open Fable.React
open Fable.React.Props



module Imprint =

  let view dispatch model =
    div [ Class "py-12 px-6 sm:px-8" ]
      [ 
        div [ Class "mx-auto max-w-2xl text-gray-700 mt-16" ]
          [
            [ "Imprint" |> ofString ] |> h2 [ Class "font-bold text-2xl  my-2" ] 
            [ "Information in accordance with section 5 TMG" |> ofString ] |> p []
            [ "KanDDDinsky UG haftungsbeschränkt" |> ofString ] |> p [ Class "font-semibold text-xl mt-4" ]
            [ "Jan Fellien (Geschäftsführender Gesellschafter)," |> ofString ] |> p []
            [ "Conrad Pöpke und Marco Heimeshoff (Gesellschafter)" |> ofString ] |> p []
            [ "Berliner Str. 101" |> ofString ] |> p []
            [ "13189 Berlin" |> ofString ] |> p []
            [ "Handelsregistereintrag: HRB 197159 B" |> ofString ] |> p [ Class "mt-2 "]
            [ "USt-IDNr: DE318608561" |> ofString ] |> p []
  
            [ "Contact" |> ofString ] |> p [ Class "font-semibold text-xl mt-8" ]
            [ "Telephone: 015201795290" |> ofString ] |> p []
            [ "E-Mail: contact@kandddinsky.com" |> ofString ] |> p []
  
            [ "Disclaimer" |> ofString ] |> p [ Class "font-semibold text-xl mt-8" ]
            [ "Accountability for content" |> ofString ] |> p [ Class "font-semibold mt-4" ]
            p [ Class "text-justify" ]
              [ "The contents of our pages have been created with the utmost care. However, we cannot guarantee the contents' accuracy, completeness or topicality. According to statutory provisions, we are furthermore responsible for our own content on these web pages. In this context, please note that we are accordingly not obliged to monitor merely the transmitted or saved information of third parties, or investigate circumstances pointing to illegal activity. Our obligations to remove or block the use of information under generally applicable laws remain unaffected by this as per §§ 8 to 10 of the Telemedia Act (TMG)." |> ofString ]
            [ "Accountability for links" |> ofString ] |> p [ Class "font-semibold mt-4" ]
            p [ Class "text-justify" ]
              [ "Responsibility for the content of external links (to web pages of third parties) lies solely with the operators of the linked pages. No violations were evident to us at the time of linking. Should any legal infringement become known to us, we will remove the respective link immediately." |> ofString ]
            
            [ "Copyright" |> ofString ] |> p [ Class "font-semibold text-xl mt-8" ]
            p [ Class "text-justify" ]
              [ "Our web pages and their contents are subject to German copyright law. Unless expressly permitted by law (§ 44a et seq. of the copyright law), every form of utilizing, reproducing or processing works subject to copyright protection on our web pages requires the prior consent of the respective owner of the rights. Individual reproductions of a work are allowed only for private use, so must not serve either directly or indirectly for earnings. Unauthorized utilization of copyrighted works is punishable (§ 106 of the copyright law)." |> ofString ]

          ]
      ]