namespace Kandddinsky


open Fable.React
open Fable.React.Props



module Photography =


  let view dispatch model =
    div [ Class "py-12 px-6 sm:px-8" ]
      [ 
        div [ Class "mx-auto max-w-2xl text-gray-700 mt-16" ]
          [
            [ "Fotohinweise" |> ofString ] |> h2 [ Class "font-bold text-2xl  my-2" ] 
            [ "1. Hinweis auf Ton- und Bildaufnahmen" |> ofString ] |> p [ Class "font-bold text-xl mt-4" ]

            p [ Class "text-justify" ]
              [ "Bei unseren Veranstaltungen werden von Referenten und Besuchern Aufnahmen in Bild und Ton angefertigt. Diese Aufnahmen können von uns zu Zwecken der Presse- und Öffentlichkeitsarbeit, der Werbung für gleichartige Veranstaltungen und unsere Aktivitäten öffentlich verbreitet und zu journalistischen Zwecken auch an Dritte weitergegeben werden." |> ofString ]
            

            [ "2. Datenschutzinformation gem. Art 13 DSGVO" |> ofString ] |> p [ Class "font-bold text-xl mt-4" ]
            [ "Datenschutzhinweise zur Verarbeitung von Aufnahmen von öffentlichen Veranstaltungen" |> ofString ] |> p []
            [ "Verantwortliche Stelle" |> ofString ] |> p [ Class "font-bold mt-4" ]
            [ "KanDDDinsky UG haftungsbeschränkt" |> ofString ] |> p [ ]
            [ "Jan Fellien (Geschäftsführender Gesellschafter)," |> ofString ] |> p []
            [ "Conrad Pöpke und Marco Heimeshoff (Gesellschafter)" |> ofString ] |> p []
            [ "Berliner Str. 101" |> ofString ] |> p []
            [ "13189 Berlin" |> ofString ] |> p []
            [ "www.kandddinsky.com & www.kandddinsky.de" |> ofString ] |> p []

            [ "Zweck" |> ofString ] |> p [ Class "font-bold mt-4" ]
            p [ Class "text-justify" ]
              [ "Anfertigung von Fotos, Bild- und Tonaufnahmen von Veranstaltungen sowie Verbreitung dieser Aufnahmen auf Webseiten, Social-Media-Kanälen sowie in eigenen Printmedien oder zur Weitergabe für redaktionelle und journalistische Nutzungen durch Dritte, zu Zwecken der Presse- und Öffentlichkeitsarbeit und Darstellung der Aktivitäten des Verantwortlichen, um den Bekanntheitsgrad des Verantwortlichen zu erhöhen und für die Teilnahme an vergleichbaren Veranstaltungen und seine Aktivitäten zu werben." |> ofString ]

            [ "Rechtsgrundlage" |> ofString ] |> p [ Class "font-bold mt-4" ]
            p [ Class "text-justify" ]
              [ "Berechtigtes Interesse im Sinne des Art. 6 Abs. 1 lit f DSGVO sowie §§ 22, 23 KUG: Presse- und Öffentlichkeitsarbeit und Darstellung der Aktivitäten des Verantwortlichen, um den Bekanntheitsgrad des Verantwortlichen zu erhöhen." |> ofString ]

            [ "Widerspruchshinweise" |> ofString ] |> p [ Class "font-bold mt-4" ]
            p [ Class "text-justify" ]
              [ "Jeder Betroffene hat das Recht, gegen die Verarbeitung Widerspruch zu erheben. Der Widerspruch kann gerichtet werden an: orga(at)kandddinsky.com oder auf jedem anderen Wege ausgeübt werden. Es ist jedoch in der Abwägung der Interessen davon auszugehen, dass regelmäßig das Interesse des Verantwortlichen an der Anfertigung und Verwendung der Aufnahmen nicht im Übermaß in die Rechte und Freiheiten eines Betroffenen eingreift, da die Bild- und Tonaufnahmen erkennbar und in der „Sozialsphäre“, also im öffentlichen Raum im Rahmen einer allgemein zugänglichen Veranstaltung, angefertigt wurden und darauf schon in der Anmeldung zur Veranstaltung und bei dieser selbst ausdrücklich hingewiesen wurde. Auch bei der Anfertigung der Bild- und Tonaufnahmen selbst wird darauf geachtet, dass keine berechtigten Interessen von abgebildeten Personen verletzt werden, die Meinungs- und Informationsfreiheit also überwiegt. Soweit dennoch aus besonderen Gründen im Einzelfall die Rechte und Freiheiten einer abgebildeten Person überwiegen sollten, werden wir im Falle eines begründeten Widerspruchs des Betroffenen die weitere Verarbeitung unterlassen. Eine Unkenntlichmachung in Printmedien, die bereits Verbreitung gefunden haben, kann nicht erfolgen. Eine Löschung von Aufnahmen in Online-Medien erfolgt im Rahmen technischer Möglichkeiten. " |> ofString ]

            [ "Speicherdauer" |> ofString ] |> p [ Class "font-bold mt-4" ]
            p [ Class "text-justify" ]
              [ "Die Daten werden nach Beendigung ihrer Nutzung oder spätestens im zweiten Jahr gelöscht. Soweit sie zu historischen Zwecken länger aufbewahrt werden, erfolgt eine entsprechende Beschränkung der Verarbeitung durch Sperrung." |> ofString ]

            [ "Kategorien von Empfängern" |> ofString ] |> p [ Class "font-bold mt-4" ]
            p [ Class "text-justify" ]
              [ "Mitarbeiter des Verantwortlichen, die mit Fragen des Vertriebs-, Marketing oder der Presse- und Öffentlichkeitsarbeit befasst sind oder im Rahmen der Verarbeitung die Daten notwendigerweise erhalten müssen (z. B. IT, sonstige Verwaltungseinheiten, Veranstaltungsorganisation). Auftragnehmer und Auftragsverarbeiter, die bei der Erstellung der Aufnahmen oder der Publikationen oder ihrer Verarbeitung und Verbreitung mitwirken. Sponsoren, Mitveranstalter und Vertreter von Presse und Rundfunk, die zu eigenen bzw. journalistisch-redaktionellen Zwecken über die Veranstaltung und die Aktivitäten des Veranstalters berichten. Steuerberater, Behörden sowie andere rechtlich Beteiligte im Falle der Notwendigkeit einer Durchsetzung von Rechten oder Abwehr von Ansprüchen oder im Rahmen von gerichtlichen oder behördlichen Verfahren. Die Aufnahmen werden durch Publikationen On- wie Offline an eine allgemeine Öffentlichkeit verbreitet bzw. zum Abruf bereitgestellt. Dabei ist es aus rechtlichen Gründen möglich, dass Dritte eigene Rechte zur Nutzung der Daten geltend machen können, beispielsweise Social-Media-Plattformen Verwertungsrechte oder Journalisten aufgrund Art. 5 GG, die ggfs. Rechten des Betroffenen entgegenstehen können. Eine Übermittlung an Empfänger in einem Drittland (außerhalb der EU) oder an eine internationale Organisation ist nicht vorgesehen. Eine automatisierte Entscheidungsfindung (Profiling) erfolgt nicht." |> ofString ]

            [ "Betroffenenrechte" |> ofString ] 
              |> p [ Class "font-bold mt-4" ]
            p [ Class "text-justify" ]
              [ "Der Betroffene ist weder vertraglich, noch gesetzlich verpflichtet, seine Daten bereitzustellen. Als von der Verarbeitung von Ton- und Bildaufnahmen betroffene Person steht Ihnen grundsätzlich das Recht auf Auskunft, Berichtigung, Löschung, Einschränkung der Verarbeitung, Widerspruch und Datenübertragbarkeit im Rahmen der gesetzlichen Bestimmungen zu (Art. 15 ff. DSGVO). Zur Ausübung Ihrer Rechte wenden Sie sich bitte an den Verantwortlichen oder den Datenschutzbeauftragten unter den oben genannten Kontaktdaten." |> ofString ]
          ]
      ]