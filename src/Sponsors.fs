namespace Kandddinsky


open Fable.React
open Fable.React.Props



module Sponsors =

  let sponsor link pic description =
    a [ Href link
        Target "_blank"
        Class "max-w-xl h-96 flex flex-col justify-center items-center shadow-md hover:shadow-xl m-4 p-6 hover:bg-gradient-to-br from-slate-50 to-blue-50" ]
      [
        img [ Class "w-full my-4"
              Src (sprintf "./img/sponsoren/%s" pic) ]
        
        description
      ]


  let innoq =
    sponsor
      "https://www.innoq.com/en/" 
      "innoq.svg" 
      ( div [ Class "text-gray-700 text-sm"]
          [
            p [ Class "text-justify" ] [ "INNOQ is a technology consulting company. Honest consulting, innovative thinking, and a passion for software development means: We deliver successful software solutions, infrastructure and products." |> ofString ] 
            p [ Class "mt-1" ] [ "We specialize in the following areas:" |> ofString ] 
            p [ Class "pl-4" ] [ "	• Strategy and Technology Consulting" |> ofString ] 
            p [ Class "pl-4" ] [ "	• Development of Digital Products and Business Models" |> ofString ] 
            p [ Class "pl-4" ] [ "	• Software Architecture and Development" |> ofString ] 
            p [ Class "pl-4" ] [ "	• Digital Platforms and Infrastructures" |> ofString ] 
            p [ Class "pl-4" ] [ "	• Knowledge Transfer, Coaching and Trainings" |> ofString ]  
          ] )

  let medialesson =
    sponsor
      "https://www.medialesson.de"
      "medialesson.png"
      ( div [ Class "text-gray-700"]
          [
            p [ Class "text-justify" ] [ "We at medialesson help companies and organizations to digital transform their business, products and processes through consulting, strategy, design, software development and training using cutting edge technology." |> ofString ] 
          ] )


  let ambar =
    sponsor
      "https://ambar.cloud/"
      "ambar.svg"
      ( div [ Class "text-gray-700"]
          [
            p [ Class "text-justify" ] [ "Ambar is a fully managed Data Streaming service that provides robust guarantees and eliminates the need for message brokers, background workers, producers, and consumers. And all it takes is 14 lines of configuration." |> ofString ] 
          ] )


  let eventstore =
    sponsor
      "https://www.eventstore.com/"
      "eventstore.png"
      ( div [ Class "text-gray-700"]
          [
            p [ Class "text-justify" ] [ "Event Store is the leading open-source operational database for Event Sourcing. It uses the state transition data model to keep the story behind your data, helping you to simplify complex systems and gain deeper insights. Perfect for building Domain Driven Designed applications. Used in Production with eCommerce, Banking, Aerospace, Insurance, Healthcare, and many more businesses across the world." |> ofString ] 
          ] )


  let avanscoperta =
    sponsor
      "https://www.avanscoperta.it/en/"
      "avanscoperta.png"
      ( div [ Class "text-gray-700"]
          [
            p [ Class "text-justify" ] [ "Avanscoperta is a growing community of professionals who are passionate about learning. We love exploring uncharted territories within software development, sharing experiences and spreading new ideas through consulting activities, training classes, events, and books." |> ofString ] 
          ] )


  let aardling =
    sponsor
      "https://www.aardling.eu/"
      "aardling.png"
      ( div [ Class "text-gray-700"]
          [
            p [ Class "text-justify text-sm" ] [ "Aardling advises software-intensive organisations on software strategy, modelling, and design. Our consultants are experts in Domain-Driven Design and related techniques like Domain Discovery, Domain Modelling, EventStorming, Context Mapping, and Team Topologies. We help you align your business strategy, product vision, and organisational structure to your software creation process." |> ofString ] 
          ] )


  let forto =
    sponsor
      "https://forto.com/en/"
      "forto.png"
      ( div [ Class "text-gray-700"]
          [
            p [ Class "text-justify text-sm" ] [ "Forto promotes the vision of a highly transparent, frictionless and sustainable supply chain. We offer transportation and technology solutions that go far beyond the movement of goods from origin to destination. Our intuitive logistics technology provides visibility and insight that enable our customers to optimize their supply chain activities. Leading brands in their industries, from fashion and furniture to electronics, trust Forto to manage the transport of their goods. We are committed to global trade and strive to increase global prosperity while driving sustainable transportation options." |> ofString ] 
          ] )


  let wps =
    sponsor
      "https://www.wps.de/"
      "wps.svg"
      ( div [ Class "text-gray-700"]
          [
            p [ Class "text-justify text-sm" ] [ "WPS Workplace Solutions GmbH is a young, innovative software development and consulting company based in Hamburg and Berlin. In an increasingly complex world, WPS's goal is to improve people's work through software. WPS is a community as well, of diversely talented people united by the joy of great software. We digitise workflows for companies and make them more productive and easier to grasp." |> ofString ] 
          ] )


  let rewe =
    sponsor
      "https://www.rewe-digital.com/en"
      "rewe.png"
      ( div [ ] [] )


  let axoniq =
    sponsor
      "https://www.axoniq.io/"
      "Axoniq.png"
      ( div [ Class "text-gray-700"]
          [
            p [ Class "text-justify text-sm" ] [ "AxonIQ offers an end-to-end development and infrastructure platform for smoothly evolving Event-Driven Architecture focused on CQRS and Event Sourcing. Our platform known as Axon includes a programming model and a specialized infrastructure to provide enterprise-ready operational support for the programming model - especially for scaling and distributing mission-critical business applications." |> ofString ] 
          ] )


  let view dispatch model =
    div [ Id "sponsoring" 
          Class "mx-auto w-full py-24" ]
      [ 
        [ "Gold Sponsor" |> ofString ] |> div [ Class "text-center text-6xl font-bold text-yellow-600 mb-4" ]
        div [ Class "flex flex-row flex-wrap justify-center items-center" ]
          [
            avanscoperta
            eventstore
            wps
          ]

        // [ "Silver Sponsors" |> ofString ] |> div [ Class "mt-20 text-center text-4xl font-semibold text-gray-500 mb-4" ]
        // div [ Class "flex flex-row flex-wrap justify-center items-center" ]
        //   [
        //     rewe
        //   ]


        [ "Video Sponsor" |> ofString ] |> div [ Class "mt-20 text-center text-5xl font-semibold text-gray-700 mb-4" ]
        div [ Class "flex flex-col items-center space-y-8" ]
          [
            ambar
          ]


        [ "Lanyard Sponsor" |> ofString ] |> div [ Class "mt-20 text-center text-5xl font-semibold text-gray-700 mb-4" ]
        div [ Class "flex flex-col items-center space-y-8" ]
          [
            medialesson
          ]

        // [ "Coffee" |> ofString ] |> div [ Class "mt-20 text-center text-4xl font-semibold text-gray-700 mb-4" ]
        // div [ Class "flex flex-col items-center space-y-8" ]
        //   [
        //     forto
        //   ]
      ]
