﻿module App 

open Elmish
open Elmish.React
open Elmish.HMR
open Elmish.Navigation
open Elmish.UrlParser
open Browser

open Router
open Kandddinsky

Fable.Core.JsInterop.importSideEffects "./index.css"


let onScroll dispatch =
  let mutable wasOnTop = false
  let handler e =
    if document.documentElement.scrollTop < 20.0 then
      if wasOnTop = false then
        wasOnTop <- true
        dispatch ScrolledToTop
    else if wasOnTop = true then
      wasOnTop <- false
      dispatch ScrolledAwayFromTop
  window.addEventListener("scroll", handler)
  { new System.IDisposable with
      member __.Dispose() = window.removeEventListener("scroll", handler) }


let subscriptions _ : Sub<Msg> =
  [ ["onScroll"], onScroll]


Program.mkProgram State.init State.update Scaffold.view  
|> Program.withSubscription subscriptions
|> Program.toNavigable (parsePath pageParser) urlUpdate
#if DEBUG
|> Program.withConsoleTrace
#endif
|> Program.withReactSynchronous "elmish-app"
|> Program.run