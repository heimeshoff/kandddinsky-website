module Router

open Elmish
open Elmish.UrlParser
open Elmish.Navigation
open Kandddinsky    

let pageParser : Parser<Page->Page,Page> =
  oneOf [
    map Landingpage (s "home" </> str)
    map Code_of_conduct (s "codeofconduct")
    map Imprint (s "imprint")
    map Photography (s "photography")
    map GDPR (s "gdpr")
  ]


let toPage = function
  | Landingpage _ -> "/home"
  | Code_of_conduct -> "/codeofconduct"
  | Imprint -> "/imprint"
  | Photography -> "/photography"
  | GDPR -> "/gdpr  "


let urlUpdate (result: Option<Page>) model =
  match result with
  | None ->
      model, Navigation.modifyUrl (toPage model.page)
  | Some page -> 
      { model with page = page }, Cmd.none