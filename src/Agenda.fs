namespace Kandddinsky


open Fable.React
open Fable.React.Props



module Agenda =


  let description =
    div [ Class "shadow-lg mt-12 py-8 px-12 bg-gradient-to-br from-slate-50 to-blue-50 z-50" ]
      [
        div [ Class "text-gray-700 font-medium text-justify py-2 z-auto"]
          [
            p [  Class "mb-4" ] [ "We are happy to announce an amazing line-up. We have balanced the agenda to bring you success stories and cautionary tales from the real world, heuristics to apply and methods and patterns to add to your toolbelt." |> ofString ]
            span [ ] [ "In the coming days we are modelling the schedule to give you the best learning journey possible. If you'd like to hear about it the moment we announce it: " |> ofString ]
            a [ Href "https://twitter.com/kandddinsky"
                Target "_blank"
                Class "text-blue-500 hover:text-blue-700 text-lg" ]
              [ "Follow us on Twitter" |> ofString ]             
          ]
      ]        


  let video_openspace = 
    embed [ Src "https://www.youtube.com/embed/oxVuBQyNTwk"
            Class "w-full"]


  let OpenSpace =
    div [ Class "shadow-lg py-8 px-12 bg-gradient-to-br from-slate-50 to-blue-50"]
      [
        div [ Class "font-bold mb-4 text-3xl text-gray-600 text-center"]
          [
            "Open Space on Day 3" |> ofString
          ]
        div [ Class "text-gray-700 text-justify space-y-4"]
          [
            div [ Class "flex flex-row flex-wrap space-y-4 sm:space-y-0"]
              [
                p [ Class "sm:w-1/2" ] [ "Join this amazing collaborative learning experience. You can book the conference and the open space seperately and shape your best learning journey. We strongly recommend to join the OpenSpace to put your fresh inspiration into practice with the help of all the speakers and community members. " |> ofString ]
                [ video_openspace ] |> div [ Class "sm:w-1/2 p-4" ] 
              ]
          ]
      ]

  let App_download = 
    div [ Class "group cursor-pointer flex flex-col items-center" ]
      [
        a [ Href "https://kandddinsky-2023.sessionize.com/"
            Target "_blank" ]
          [
            div [ Class "text-center font-semibold text-2xl text-[#57b3d1] group-hover:text-[#217e9c]"]
              [ "Download the App" |> ofString ]
          ]
      ]



  let view dispatch model =
    div [ Id "agenda" 
          Class "mx-auto max-w-5xl w-full py-24" ]
      [ 


        [ "Agenda" |> ofString ] |> div [ Class "text-center text-6xl font-bold text-gray-700 mb-8" ]

        App_download

//        OpenSpace

        // description // publish when speakers are know but before the schedule is finished

        div [ Id "embed_target" 
              Class "mt-8 sticky" ] []

      ]
