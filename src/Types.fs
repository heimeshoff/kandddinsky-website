namespace Kandddinsky

open Fable.React

type Page =
  | Landingpage of string
  | Code_of_conduct
  | Imprint
  | Photography
  | GDPR


type Workshop = 
  {
    Img : string
    Trainer : string
    Title : string
    Abstract : string
    Details : ReactElement // TODO: Load this dynamically from .MD files
  }


type Msg = 
  | Menu_Msg of Menu.Msg
  | Navigate_to of Page
  | ScrolledToTop
  | ScrolledAwayFromTop
  | OnLogError of exn
  | Show_workshop_details of Workshop
  | Close_workshop_details


type Model = {
  menu_model : Menu.Model
  page: Page
  scrolled_to_top : bool
  workshop_details : Workshop option
}
