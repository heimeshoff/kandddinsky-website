namespace Kandddinsky


open Fable.React
open Fable.React.Props



module Cfp =

  let view dispatch model =
    div [ Id "cfp"
          Class "py-24 px-2 sm:px-6" ]
      [ 
        div [ Class "mx-auto max-w-2xl leading-relaxed overflow-hidden sm:overflow-visible" ]
          [
            [ "Call for proposals" |> ofString ] |> p [ Class "mb-6 text-3xl font-bold text-gray-600" ]
            [ "We want your session on the art of business software," |> ofString ] |> p [ Class "" ]
            [ "from Domain Driven Design to Agile, Architecture and beyond." |> ofString ] |> p [ Class "" ]

            [ "Format" |> ofString ] |> p [ Class "mt-6 mb-4  text-2xl font-semibold text-gray-600" ]
            ol [ Class "list-disc pl-4" ]
              [
                [ "Presentation (50 min)" |> ofString ] |> li []
                [ "Hands-On Session (2 h)" |> ofString ] |> li []
                // [ "Full-Day Workshop" |> ofString ] |> li []
              ]
            
            [ "Topics" |> ofString ] |> p [ Class "mt-6 mb-4  text-2xl font-semibold text-gray-600" ]
            ol [ Class "list-disc pl-4" ]
              [
                [ "DDD experience stories, success and failures alike" |> ofString ] |> li []
                [ "Collaborative modelling methods" |> ofString ] |> li []
                [ "Socio-technical systems" |> ofString ] |> li []
                [ "Software architecture" |> ofString ] |> li []
                [ "Semantics in code" |> ofString ] |> li []
                [ "Testing" |> ofString ] |> li []
                [ "Heuristics" |> ofString ] |> li []
                [ "Product management" |> ofString ] |> li []
                [ "Visualisation and story-telling" |> ofString ] |> li []
                [ "Event Driven Distributed Systems" |> ofString ] |> li []
                [ "and more ..." |> ofString ] |> li []
              ]

            [ "Any topic that you deem fit to make the world of business software development better is welcome. We are happy to jump on a call with you and discuss / refine your ideas. When in doubt if your talk fit's our programm, submit and let's discuss." |> ofString ] |> p [ Class "my-4" ]

            div [ Class "flex mt-12 space-x-2" ]
              [                    
                div [ Class "w-1/2 sm:w-64 flex justify-center items-center"]
                  [
                    a [ Class "p-6 font-bold text-gray-50 hover:text-gray-100 bg-[#f28705] hover:bg-[#bf6a03]"
                        Href "https://sessionize.com/kandddinsky-2024" 
                        Target "_blank" ]
                      [
                        "Submit your proposal" |> ofString
                      ]
                  ]

                img [ Class "w-1/2 sm:w-64 flex justify-center items-center"
                      Src "./img/design/dinsky_pfeil_4.png" ]
              ]

          ]
      ]