namespace Kandddinsky


open Fable.React
open Fable.React.Props
open SVGs


module Tickets =

  type Focus =
    | Main
    | Other


  let pretixLink = "https://pretix.eu/kandddinsky/2024/"


  let calendarday month day rotation =
    div [ Class (sprintf "flex flex-col w-48 lg:w-64 rounded-sm shadow-lg m-2 %s" rotation) ]
      [
        div [ Class "bg-red-500 text-center text-white text-3xl py-1"]
          [
            month |> ofString
          ]

        div [ Class "bg-white text-center font-bold text-gray-700 text-4xl py-6"]
          [
            day |> ofString
          ]
      ]


  let title title =
    [ title |> ofString ] |> div [ Class "whitespace-nowrap text-4xl font-bold text-gray-700 my-4" ]


  let calendardays days =
    div [ Class "flex mb-4" ]  
      days


  let pricelink price =
    a [ Class "font-bold text-4xl text-[#57b3d1]  hover:text-[#217e9c] ml-2 mt-2 cursor-pointer whitespace-nowrap" 
        Href pretixLink 
        Target "_blank" ]
      [ price |> ofString ]


  let mainconference =
    div [ Class "flex flex-col items-center" ]
      [
        title "Main Conference"

        calendardays
          [ calendarday "Oct." "04.-05." "-rotate-3" ]
        
        [ pricelink "550 €" ] |> div [ Class "text-4xl flex flex-wrap items-center justify-center" ]
      ]


  let openspace =
    div [ Class "flex flex-col items-center" ]
      [
        title "Open Space"
        
        calendardays
          [ calendarday "Oct." "06."  "rotate-2" ]

        pricelink "250 €"

      ]


  let video_2019 = 
    iframe [ Src "https://www.youtube.com/embed/InsLfYeMjYc"
             Title "YouTube video player"
             FrameBorder 0
             HTMLAttr.Custom ("allow", "accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture") ]
      [ ]

  let video_2022 = 
    iframe [ Src "https://youtu.be/206bpiozkS4"
             Title "YouTube video player"
             FrameBorder 0
             HTMLAttr.Custom ("allow", "accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture") ]
      [ ]
      

  let KanDDDinskyIsBack =
    div [ Class "shadow-lg py-8 px-12 bg-gradient-to-br from-slate-50 to-blue-50"]
      [
        div [ Class "font-bold mb-4 text-3xl text-gray-600 text-center"]
          [
            "KanDDDinsky is back" |> ofString
          ]
        div [ Class "text-gray-700 text-justify space-y-4"]
          [
            p [ Class "mt-4" ] [ "Germany's first Domain-Driven Design conference will take place for the 5th. time this year. To celebrate, we are combining our usual format of practical hands-on sessions and inspiring talks by industry leaders with an additional Open Space day." |> ofString ]
            div [ Class "flex flex-row flex-wrap space-y-4 sm:space-y-0"]
              [
                p [ Class "sm:w-1/2" ] [ "Join this amazing collaborative learning experience. You can book the conference and the open space seperately and shape your best learning journey. We strongly recommend to join the OpenSpace to put your fresh inspiration into practice with the help of all the speakers and community members. " |> ofString ]
                [ video_2019 ] |> div [ Class "sm:w-1/2 flex justify-center" ] 
              ]
          ]

        div [ Class "flex mt-12 justify-center space-x-2" ]
          [                    
            img [ Class "w-1/2 sm:w-64 flex justify-center items-center"
                  Src "./img/design/dinsky_pfeil_1.png" ]

            div [ Class "w-1/2 sm:w-64 flex justify-center items-center"]
              [
                a [ Class "p-6 font-bold text-gray-50 hover:text-gray-100 bg-[#57b3d1] hover:bg-[#217e9c]"
                    Href pretixLink 
                    Target "_blank" ]
                  [
                    "Get your tickets!" |> ofString
                  ]
              ]
          ]
      ]


  let checkPoint text =
    div [ Class "flex justify-start space-x-2"]
      [
        [ svg_check ] |> div [ Class "text-[#1c98c1]" ]
        [ text |> ofString ] |> div [ Class "text-gray-700" ]
      ]


  let pricePoint focus label tagline price checkpoints = 
    let buttonCSS = 
      match focus with
      | Main -> "text-gray-50 hover:text-gray-100 bg-[#1c98c1] hover:bg-[#146f8e]"
      | Other -> "text-[#1c98c1] hover:text-[#146f8e] border-2 border-[#1c98c1] hover:border-[#146f8e] bg-transparent"


    div [ Class "border-2 border-[#1c98c1] p-8 w-96 flex-shrink-0"]
      [ 
        [ label |> ofString ] |> p [ Class "text-xl font-semibold text-[#1c98c1]" ]
        [ tagline |> ofString ] |> p [ Class "text-gray-700 mt-2 mb-8" ]
        
        [ price ] |> div [ Class "w-full flex justify-center" ]
          

        a [ Href pretixLink 
            Target "_blank" ]
          [ [ "Buy ticket" |> ofString ] |> div [ Class (sprintf "w-full my-8 p-4 text-center font-bold %s shadow-sm" buttonCSS) ] ]


        div [ Class "flex flex-col space-y-2"]
          checkpoints
      ]



  let view dispatch model =
    div [ Id "tickets" 
          Class "py-24 px-2 sm:px-6" ]
      [ 
        div [ Class "mx-auto max-w-4xl overflow-hidden sm:overflow-visible space-y-24" ]
          [
            div [ Class "w-full flex flex-col items-stretch  sm:flex-row justify-around space-y-8 sm:space-y-0 sm:space-x-8 " ]
              [
                pricePoint 
                  Main
                  "Conference" 
                  "Oct 28.-29. Talks and Hands-On Sessions"
                  
                  ( [ [ "€ 600" |> ofString ] |> span [ Class "text-3xl font-bold"]
                      [ "/Full price € 800" |> ofString ] |> span [ Class "ml-4 text-xl font-semibold line-through"] 
                    ] |> div [ ] )
                  
                  [ 
                    "Blind Bird Tickets until April 14., save € 200,-" |> checkPoint
                    // "Early Bird Tickets until June 3. '23, save € 100,-" |> checkPoint
                    "Talks and Hands-On Sessions" |> checkPoint
                    "Free attendance to evening reception" |> checkPoint ]

                pricePoint 
                  Other
                  "Open Space" 
                  "Oct. 30. Community driven agenda"
                  
                  ( [ [ "€ 200" |> ofString ] |> span [ Class "text-3xl font-bold"]
                    ] |> div [ ] )
                  
                  [ "Build your own Agenda" |> checkPoint
                    "A full day made of hallway tracks" |> checkPoint 
                    "Six rooms, infinite modelling space" |> checkPoint 
                    "Dynamic modelling workshops, talks, coding and discussion groups with the DDD community" |> checkPoint ]
              ]
          ]
      ]