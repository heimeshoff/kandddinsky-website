namespace Kandddinsky



module State =
  
  open Elmish
  open Elmish.Navigation
  open Router
  open API


  let init result =  
    let (model, cmd) = 
      urlUpdate result 
        { menu_model = Menu.State.init
          page = Landingpage "top"
          scrolled_to_top = true
          workshop_details = None
        }
    model, Cmd.none  


  let update msg (model:Kandddinsky.Model) = 
    match msg with
    | Menu_Msg m -> 
        let (new_menu_model, cmd) = Menu.State.update m model.menu_model
        { model with menu_model = new_menu_model}, cmd

    | Navigate_to target -> 
        let hash = 
          match target with
          | Landingpage t -> t
          | _ -> "top"

        { model with 
            menu_model = Menu.State.init
            page = target
             }, 
          Cmd.batch [ Navigation.modifyUrl (toPage target) ; Cmd.OfFunc.attempt scrollIntoView hash OnLogError ]
        
    | ScrolledToTop ->        
        { model with scrolled_to_top = true }, Cmd.none
        
    | ScrolledAwayFromTop ->        
        { model with scrolled_to_top = false }, Cmd.none
        
    | OnLogError e ->
        model, Cmd.none

    | Show_workshop_details workshop ->
        { model with workshop_details = Some workshop }, Cmd.none

    | Close_workshop_details ->
        { model with workshop_details = None }, Cmd.none



