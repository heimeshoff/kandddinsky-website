namespace Kandddinsky


open Fable.React
open Fable.React.Props



module Workshops =


  let Kenny =
    {
      Img = "./img/2023/kenny_marco.jpg"
      Trainer = "Kenny Baas-Schwegeler & Marco Heimeshoff"
      Title = "Navigating Conflicts in Software Architecture Decision Making"
      Abstract = "Software architecture forms the backbone to building successful software systems, but the decision-making process can be a hassle to navigate due to complexity and conflicts..."
      Details = 
        div [ Class "p-4"] 
          [ 
            p [ Class "mb-4" ] [ "Software architecture forms the backbone to building successful software systems, but the decision-making process can be a hassle to navigate due to complexity and conflicts. These conflicts can originate from a lack of complete information to emotionally charged personal preferences, leading to decisions that may not fully align with the system's overall objectives. This misalignment can result in a less than ideal software architecture for the business and it also can leave the team members unhappy and resist the decision. Our goal should be to ensure decisions are sustainable – benefiting both the technical needs and the human needs involved." |> ofString ]
            p [ Class "mb-4" ] [ "Drawing from my co-authored book “Collaborative Software Design”, this concise one-day workshop is focused on understanding conflicts during collaborative modelling and architectural decision-making and navigating them effectively." |> ofString ]
            p [ Class "mt-4 border-b-2 font-medium" ] [ "In this workshop, you will:" |> ofString ]
            p [ Class "mb-4" ] [ "* Get an overview of the importance of collaborative modelling for clear communication and its role in sustainable software design." |> ofString ]
            p [ Class "mb-4" ] [ "* Delve deep into the heart of decision-making conflicts in software architecture and how they can hamper the overall system design." |> ofString ]
            p [ Class "mb-4" ] [ "* Explore the concept of \"role theory\" from Deep Democracy, understanding how it forms the basis for inclusive and sustainable decision-making." |> ofString ]
            p [ Class "mb-4" ] [ "* Learn practical techniques to manage conflicts, ensuring every voice is heard, and the minority opinion is incorporated." |> ofString ]
            p [ Class "mb-4" ] [ "This workshop is a must for software engineers, architects, tech leads, and managers who are looking to elevate their decision-making processes, ensuring they are effective, inclusive, and sustainable. Join us to navigate the intricate maze of software architecture decision-making with clarity and confidence." |> ofString ]
          ]
    }


  let Consolaro =
    {
      Img = "./img/2023/marco-ale-milano.jpg"
      Trainer = "M. Consolaro & A. Di Gioia"
      Title = "From Eventstorming to ATDD passing by Hexagonal Architecture. Go live on your first day on our team!"
      Abstract = "Imagine you are going to start working on a new team: this is your first day in your new job. How do you expect it to be? Boring? Would you be able to write code and deploy to production on your very first day?..."
      Details = 
        div [ Class "p-4"] 
          [ 
            p [ Class "mb-2" ] [ "Imagine you are going to start working on a new team: this is your first day in your new job. How do you expect it to be? Boring? Would you be able to write code and deploy to production on your very first day?" |> ofString ]
            p [ Class "mb-2" ] [ "In this hands-on workshop, you will join our team and work with us for one day in a production-like environment we prepared. The goal is to deploy one story to production by the end of the day. Impossible?" |> ofString ]
            p [ Class "mb-2" ] [ "We will start by explaining the context of our Car Finance Business. Using Core Domain charts, bounded contexts mapping, big picture Eventstorming, Team Topologies, C4, and some custom Hexagonal Architecture class and sequence diagrams, we will show how to leverage the artefacts for onboarding new team members fast." |> ofString ]
            p [ Class "mb-2" ] [ "In the second part of the workshop, we will start writing code collaboratively with an acceptance test. We will help you to test-drive the user story on an enterprise-like application with live databases and APIs, Service Bus and event messaging, trunk-based development, continuous integration&deployment on Azure, and information radiators." |> ofString ]
            p [ Class "mb-2" ] [ "We will write Java code collaboratively in a mob/ensemble \"fishbowl\" programming and focus on the most important properties of an architecture: modularity, loose coupling, and business alignment." |> ofString ]
            p [ Class "mb-2" ] [ "Sit within the mob and help navigate the development of the solution, or just relax among the audience watching the progress, and asking questions in real-time. Impossible is nothing!" |> ofString ]
 
            p [ Class "mb-2 border-b-2 font-medium" ] [ "Notes" |> ofString ]

            p [] [ "No laptop is required for the audience to participate. " |> ofString ]
          ]
    }


  let Mufrid =
    {
      Img = "./img/2023/mufrid.jpg"
      Trainer = "Mufrid Krilic"
      Title = "From Business Goals to Software Requirements - with Impact Mapping and Domain Storytelling"
      Abstract = "Building software that fulfills business goals is the core task of software development teams. To turn business goals into software, stakeholders must have shared artifacts to align their perspectives and priorities...."
      Details = 
        div [ Class "p-4"] 
          [ 
            p [ Class "mb-2" ] [ "Building software that fulfills business goals is the core task of software development teams. To turn business goals into software, stakeholders must have shared artifacts to align their perspectives and priorities. Those artifacts have many names: requirements, features, user stories, use cases, specifications, etc." |> ofString ]

            p [ Class "mb-2" ] [ "As DDD practitioners, we believe that software requirements are best created as a result of a collaborative modeling process. Impact Mapping and Domain Storytelling are collaborative modeling methods that support conversations about requirements. They help you to approach requirements from two different angles:" |> ofString ]
            p [ Class "mb-2" ] [" - Linking business goals with impacts that a software product has on its users." |> ofString ]
            p [ Class "mb-2" ] [" - Designing cohesive and viable business processes as Domain Stories." |> ofString ]

            p [ Class "mb-2" ] [" In this workshop, we will show you how to bridge the gap between the business goals and deliverables by:" |> ofString ]
            p [ Class "mb-2" ] [" - learning Impact Mapping and Domain Storytelling" |> ofString ]
            p [ Class "mb-2" ] [" - practicing these methods on a case study in small groups" |> ofString ]
            p [ Class "mb-2" ] [" - learning to have conversations about WHY you need to build WHAT" |> ofString ]
            p [ Class "mb-2" ] [" - learning how to turn individual requirements into business processes" |> ofString ]

            p [ Class "mb-2" ] [" Finally, we will give you some pointers on how to organize requirements and drill down to implementable domain models." |> ofString ]

            p [ Class "mb-2 border-b-2 font-medium" ] [ "Notes" |> ofString ]

            p [] [ "No laptop is required for the audience to participate. " |> ofString ]
          ]
    }


  let workshop dispatch (workshop:Workshop) =
    div [ Class "m-4 w-80 rounded-md shadow-md bg-white flex flex-col justify-between" ] 
      [
        div []
          [
            div [ Class "h-32 overflow-hidden flex justify-center items-center" ]
              [ img [ Class "object-cover" ; Src workshop.Img ] ]
            
            div [ Class "p-4 text-gray-700"]
              [
                [ workshop.Title |> ofString ] |> div [ Class "font-bold text-lg" ] 
                [ workshop.Trainer |> ofString ] |> div [ Class "text-gray-500" ]  
                [ workshop.Abstract |> ofString ] |> div [ Class "text mt-2" ] 
              ]
          ]
        div [ Class "flex flex-row" ]
          [
            [ "Details" |> ofString ] 
              |> div [ Class "w-1/2 h-12 border border-slate-300 hover:bg-gray-200 cursor-pointer flex justify-center items-center" 
                       OnClick (fun _ -> dispatch (Show_workshop_details workshop) ) ]
            [ "Tickets" |> ofString ] |> a [ Href "https://pretix.eu/kandddinsky/2023/" ; Target "_blank" ; Class "w-1/2 h-12 border border-slate-300 text-gray-50 hover:text-gray-100 bg-[#57b3d1] hover:bg-[#217e9c] cursor-pointer flex justify-center items-center" ]
              
          ]
      ]


  let details dispatch workshop =
    match workshop with 
    | Some w -> 
        div [ Class "fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity z-50 flex items-center justify-center"
              OnClick (fun _ -> dispatch Close_workshop_details ) ]
          [ 
            div [ Class "bg-white text-left shadow-xl sm:my-8 sm:w-full sm:max-w-lg flex flex-col justify-stretch max-h-screen" 
                  OnClick (fun e -> e.stopPropagation()) ]
              [ 
                img [ Class "h-40 sm:h-64 object-cover w-full" ; Src w.Img ]
                div [ Class "h-18 py-4 px-8 text-gray-700"]
                  [
                    [ w.Title |> ofString ] |> div [ Class "font-bold text-lg" ] 
                    [ w.Trainer |> ofString ] |> div [ Class "text-gray-500" ]  
                  ]
                  
                div [ Class "flex-grow p-4 text-gray-700 overscroll-contain overflow-y-auto max-h-96" ]
                  [ w.Details ] 

                div [ Class "h-12 flex flex-row" ]
                  [
                    [ "Close" |> ofString ] 
                      |> div [ Class "w-1/2 h-12 border border-slate-300 hover:bg-gray-200 cursor-pointer flex justify-center items-center" 
                               OnClick (fun _ -> dispatch Close_workshop_details ) ]
                    
                    [ "Tickets" |> ofString ] 
                      |> a [ Href "https://pretix.eu/kandddinsky/2023/" 
                             Target "_blank" 
                             Class "w-1/2 h-12 border border-slate-300 text-gray-50 hover:text-gray-100 bg-[#57b3d1] hover:bg-[#217e9c] cursor-pointer flex justify-center items-center" ]
                  ]
              ]
          ]

    | None -> div [ Class "hidden" ] []


  let workshops dispatch (model:Model) = 
    div [ Class "relative my-10"]
      [
        div [ Class "flex flex-row justify-center flex-wrap" ]
          [
            workshop dispatch Kenny
            workshop dispatch Consolaro
            workshop dispatch Mufrid
            // workshop dispatch Heimeshoff
          ]
  
        details dispatch model.workshop_details
      ]


  let view dispatch (model:Model) =
    div [ Id "workshops" 
          Class "py-24 px-2 sm:px-6 mx-auto overflow-hidden sm:overflow-visible" ]
      [ 
        div [ Class "max-w-4xl mx-auto"]
          [
            [ "Pre-conference Workshops" |> ofString ] |> div [ Class "text-center text-6xl font-bold text-gray-700 mb-8" ]

            div [ Class "text-gray-700 text-justify space-y-4 p-2"]
              [
                span [ Class "mt-4" ] [ "On " |> ofString ]
                span [ Class "font-bold" ] [ "Tuesday, October 3. " |> ofString ]
                span [ Class "mt-4" ] [ "we offer a selection of full day deep dives at the conference venue. Get your mind immersed in these topics. " |> ofString ]
                span [ Class "mt-4" ] [ "Order your full day workshop starting from " |> ofString ]
                // span [ Class "font-bold" ] [ " 749 EUR" |> ofString ]
                // span [ Class "mt-4" ] [ " early bird and " |> ofString ]
                span [ Class "font-bold" ] [ " 649 EUR" |> ofString ]
                // span [ Class "mt-4" ] [ " full price when you order in October. " |> ofString ]
              ]
          ]

        workshops dispatch model

        div [ Class "max-w-4xl mx-auto flex justify-center" ]
          [                    
            img [ Class "w-4/5 flex justify-center items-center"
                  Src "./img/design/dinsky_sticky.png" ]
          ]

      ]