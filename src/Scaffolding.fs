namespace Kandddinsky



module Scaffold =

  open Fable.React
  open Fable.React.Props


  let menu dispatch model = 

    let home =
      img [ Class "h-16 cursor-pointer transition-opacity duration-700"
            Src "./img/design/kandddinsky_without_claim.svg" 
            OnClick (fun _ -> Navigate_to (Landingpage "top") |> dispatch) ]

    let agenda =
      div [ Class "my-auto p-2 text-2xl text-gray-700 border-b-2 border-transparent hover:border-orange-400 transition-colors cursor-pointer"
            OnClick (fun _ -> Navigate_to (Landingpage "agenda") |> dispatch)
          ]
        [ "Agenda" |> ofString ]


    let workshops =
      div [ Class "my-auto p-2 text-2xl text-gray-700 border-b-2 border-transparent hover:border-orange-400 transition-colors cursor-pointer"
            OnClick (fun _ -> Navigate_to (Landingpage "workshops") |> dispatch)
          ]
        [ "Pre-Workshops" |> ofString ]


    let venue =
      div [ Class "my-auto p-2 text-2xl text-gray-700 border-b-2 border-transparent hover:border-orange-400 transition-colors cursor-pointer"
            OnClick (fun _ -> Navigate_to (Landingpage "venue") |> dispatch)
          ]
        [ "Venue" |> ofString ]

    let tickets =
      div [ Class "my-auto p-2 text-2xl text-gray-700 border-b-2 border-transparent hover:border-orange-400 transition-colors cursor-pointer"
            OnClick (fun _ -> Navigate_to (Landingpage "tickets") |> dispatch)
          ]
        [ "Tickets" |> ofString ]

    let cfp =
      div [ Class "my-auto p-2 text-2xl text-gray-700 border-b-2 border-transparent hover:border-orange-400 transition-colors cursor-pointer"
            OnClick (fun _ -> Navigate_to (Landingpage "cfp") |> dispatch)
          ]
        [ "Cfp" |> ofString ]

    let sponsoring =
      div [ Class "my-auto p-2 text-2xl text-gray-700 border-b-2 border-transparent hover:border-orange-400 transition-colors cursor-pointer"
            OnClick (fun _ -> Navigate_to (Landingpage "sponsoring") |> dispatch)
          ]
        [ "Sponsors" |> ofString ]


    Menu.View.render 
      (Menu_Msg >> dispatch)
      model.scrolled_to_top 
      home
      tickets
      cfp
      agenda
      workshops
      venue
      sponsoring
      model.menu_model   


  let view model dispatch =
    let scrolling_behaviour = 
      match model.workshop_details with
      | Some _ -> "overflow-hidden"
      | None -> ""

    div [ Id "top"
          Class "relative" ]
      [ 
        menu dispatch model
        
        match model.page with
        | Landingpage target -> 
            div [ Class scrolling_behaviour ]
              [
                Hero.view dispatch model
                Divider.first
                Tickets.view dispatch model
                // Workshops.view dispatch model
                Divider.second
                Cfp.view dispatch model
                // Agenda.view dispatch model
                // Venue.view dispatch model
                // Sponsors.view dispatch model
              ]

        | Code_of_conduct -> Code_of_conduct.view dispatch model

        | Imprint -> Imprint.view dispatch model 

        | Photography -> Photography.view dispatch model

        | GDPR -> GDPR.view dispatch model

        Footer.view dispatch
      ]