## Voraussetzungen
- [.NET 5](https://dotnet.microsoft.com/en-us/download/dotnet/5.0)
- [node.js](https://nodejs.org/en/)
- [yarn 1.x](https://classic.yarnpkg.com/lang/en/)

## Start
Zu Beginn alle Abhängigkeiten installieren.
```bash
yarn install
dotnet restore src
```

## Entwicklungsmodus
```bash
yarn dev
```
Startet einen lokalen Server (http://localhost:3000) und aktualisiert live den Inhalt, sobald sich der Quellcode ändert (Hot module replacement)

## Bauen für die Produktion
```bash
yarn build
```
Erzeugt einen `dist` Ordner, der alle Dateien für die Web-Anwendung enthält. Die Dateien sind optimiert auf eine schnelle Performance und kleine Dateigröße.

```bash
yarn preview
```
Nach dem erfolgreichen Erstellen der Produktiv-Anwendung, kann sich das Ergebnis mit Preview angeschaut werden.
